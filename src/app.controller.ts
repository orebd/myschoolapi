import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }
}



import { IsNumberString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ValidId {
  @ApiProperty()
  @IsNumberString()
  id: number;
}