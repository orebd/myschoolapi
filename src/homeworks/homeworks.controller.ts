import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { HomeworksService } from './homeworks.service';
import { CreateHomeworkDto } from './dto/create-homework.dto';
import { UpdateHomeworkDto } from './dto/update-homework.dto';
import { ApiTags } from '@nestjs/swagger';
import { ValidId } from '../app.controller'

@ApiTags('homeworks')
@Controller('homeworks')
export class HomeworksController {
  constructor(private readonly homeworksService: HomeworksService) {}

  @Post()
  create(@Body() createHomeworkDto: CreateHomeworkDto) {
    return this.homeworksService.create(createHomeworkDto);
  }

  @Get()
  findAll() {
    return this.homeworksService.findAll();
  }

  @Get(':id')
  findOne(@Param() params: ValidId) {
    // findOne(@Param('id') id: number) {
      // return this.homeworksService.findOne(+id);
    return this.homeworksService.findOne(+params.id);
  }

  @Put(':id')
  update(@Param() params: ValidId, @Body() updateHomeworkDto: UpdateHomeworkDto) {
    return this.homeworksService.update(+params.id, updateHomeworkDto);
  }

  @Delete(':id')
  remove(@Param() params: ValidId) {
    return this.homeworksService.remove(+params.id);
  }
}
